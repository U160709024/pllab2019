// Example program
#include <iostream>
#include <string>

class Point
{
   private:
    int x, y;
   public:
    Point(int, int);
    void assignX(int);
    int printX();
    int printY();

};
   // the constructor function
   Point::Point(int a, int b)
   {
    x=a;y=b;
   }
   void Point::assignX(int a)
   {
    x=a;
   }
int Point::printX()
   {
    return x;
   }
int Point::printY()
   {
    return y;
   }
int main(){
    int a=3,b=7;
    a=b;b=9;
    Point p1(a,b);
    Point p2=p1;
    p1.assignX(123);
    std::cout <<p1.printX()<<" "<<p1.printY()<<"\n";
    std::cout <<p2.printX()<<" "<<p2.printY()<<"\n";
    return 0;
}